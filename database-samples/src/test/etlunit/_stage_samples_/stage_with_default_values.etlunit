@Database(id: 'edw')
class stage_with_default_values {
	@Description(
		description: [
			'Post data from a data file which contains only one column, ID, ',
			'and uses default values for the rest.  This is accomplished by',
			'specifying default-value in the relational fml.',
			'The relational fml in this case is edw-PUBLIC-TEST_TABLE'
		]
	)
	@Test
	stageDataSetWithDefaults(){
		stage(
			source: 'ID_DATA_FILE',
			reference-file-type: 'id',
			target-table: 'TEST_TABLE'
		);

		// run an assertion to verify we got it right
		assert(
			source-table: 'TEST_TABLE',
			target: 'ID_WITH_DEFAULT'
		);
	}

	@Description(
		description: [
			'Post data into a table using a minimal FML and using a relational FML',
			'which publishes default values with sequences.  This allows for data',
			'sets which are unique in case there are constraints on fields we',
			'dont care about.  Check the ddl for TEST_TABLE_II in the database scripts.',
			'Every column has a unique index which would break with straight default values.'
		]
	)
	@Test
	stageDataSetWithDefaultSequences(){
		stage(
			source: 'ID_II_MINIMAL',
			reference-file-type: 'id',
			target-table: 'TEST_TABLE_II'
		);

		// run an assertion so we can see the default values
		assert(
			source-table: 'TEST_TABLE_II',
			target: 'ID_WITH_SEQUENCES'
		);
	}

	@Test
	stageWithNumericPrecisionAndScale()
	{
		stage(
			source: 'ID_II_MINIMAL',
			reference-file-type: {
				column-list-mode: 'include',
				column-list: [
					'ID'
				]
			},
			target-table: 'TEST_TABLE_NUMERICS'
		);

		// run an assertion so we can see the default values
		assert(
			source-table: 'TEST_TABLE_NUMERICS',
			target: 'NUMERIC_WITH_SEQUENCES'
		);
	}

	@Test
	stageDatesAndTimes()
	{
		stage(
			source: 'ID_II_MINIMAL',
			reference-file-type: {
				column-list-mode: 'include',
				column-list: [
					'ID'
				]
			},
			target-table: 'TEST_TABLE_DATES'
		);

		// run an assertion so we can see the default values
		assert(
			source-table: 'TEST_TABLE_DATES',
			target: 'DATE_WITH_SEQUENCES'
		);
	}

	@Test
	projectDefaultFml()
	{
		stage(
			source: 'ID_REF_MINIMAL_EDW1',
			target-table: 'TEST_TABLE_REFS'
		);

		assert(
			source-table: 'TEST_TABLE_REFS',
			target-reference-file-type: {version: 'edw.2'},
			target: 'ID_REF_MINIMAL_EDW2'
		);
	}
}